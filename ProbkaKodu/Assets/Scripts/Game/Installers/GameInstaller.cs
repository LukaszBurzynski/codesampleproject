﻿using UnityEngine;
using Zenject;

namespace CodeSample.Game
{
    /// <summary>
    /// Zenject installer, not correctly created but works for this demonstration.
    /// </summary>
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private Game _game;
        [SerializeField] private GameUpdater _gameUpdater;
        [SerializeField] private PlayerController _player;

        public override void InstallBindings()
        {
            Container.Bind<ILifecycle>().To<Game>().FromInstance(_game).AsSingle().NonLazy();
            Container.Bind<IPauseable>().To<Game>().FromInstance(_game).AsSingle().NonLazy();

            Container.Bind<IGameStages>().To<PlayerController>().FromInstance(_player).AsSingle().NonLazy();

            Container.Bind<IUpdater>().To<GameUpdater>().FromInstance(_gameUpdater).AsSingle().NonLazy();
        }
    }
}
