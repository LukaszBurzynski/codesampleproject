﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CodeSample.Game
{
    /// <summary>
    /// Used to spawn Player's, created as supporting code for demonstration purposes.
    /// </summary>
    public class Spawner : MonoBehaviour
    {
        [SerializeField] private int _playersToSpawn = 10;

        private IUpdater _updater;
        private IPauseable _pauseable;
        private ILifecycle _lifecycle;

        private List<Player> _players = new List<Player>();

        [Inject]
        public void Construct(ILifecycle lifecycle, IPauseable pauseable, IUpdater updater)
        {
            _lifecycle = lifecycle;
            _pauseable = pauseable;
            _updater = updater;
        }

        private void Start()
        {
            for (var i = 0; i < _playersToSpawn; i++)
            {
                var newPlayer = new Player(i, _lifecycle, _pauseable, _updater) {Enabled = false};
                _players.Add(newPlayer);
            }
        }
    }
}
