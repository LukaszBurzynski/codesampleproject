﻿using UnityEngine;
using Zenject;

namespace CodeSample.Game
{
    /// <inheritdoc />
    /// <summary>
    /// Supporting code for CustomStyleGame, created to show how pausing works on objects.
    /// </summary>
    public class Player : BaseGameObject
    {
        private readonly int _id = 0;

        private Vector3 _position = Vector3.zero;
        private float _playerSpeed = 10f;

        [Inject]
        public Player(int id, ILifecycle lifecycle, IPauseable pausable, IUpdater updater) :
            base(lifecycle, pausable, updater)
        {
            _id = id;
        }

        public override void Update()
        {
            Move();
        }

        /// <summary>
        /// Dumy player movement for demonstration purposes
        /// </summary>
        private void Move()
        {
            var direction = Vector3.right;
            _position += direction * _playerSpeed * Time.deltaTime;
            Debug.Log(string.Format("Player {0} position {1}", _id, _position));
        }
    }
}
