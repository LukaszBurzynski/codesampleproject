﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeSample.Game
{
    /// <summary>
    /// The purpose of GameUpdater is to create custom Update function to not use Unity Update.
    /// To use it script needs to inherit after IUpdateable interface and Register to GameUpdater.
    /// </summary>
    public class GameUpdater : MonoBehaviour, IUpdater
    {
        private readonly List<IUpdateable> _objectsToUpdate = new List<IUpdateable>();

        /// <summary>
        /// Call Register to activate Update from IUpdateable interface.
        /// </summary>
        /// <param name="objectToUpdate"></param>
        public void Register(IUpdateable objectToUpdate)
        {
            _objectsToUpdate.Add(objectToUpdate);
        }

        /// <summary>
        /// Call Unregister to deactivate Update from IUpdateable interface.
        /// </summary>
        /// <param name="objectToUpdate"></param>
        public void Unregister(IUpdateable objectToUpdate)
        {
            _objectsToUpdate.Remove(objectToUpdate);
        }

        private void Update()
        {
            for (var i = _objectsToUpdate.Count - 1; i >= 0; i--)
            {
                if (_objectsToUpdate[i].Enabled)
                {
                    _objectsToUpdate[i].Update();
                }
            }
        }
    }
}
