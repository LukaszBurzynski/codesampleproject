﻿namespace CodeSample.Game
{
    /// <summary>
    /// Interface for calling Update in Update engine.
    /// </summary>
    public interface IUpdateable
    {
        bool Enabled { get; set; }
        void Update();
    }
}
