﻿namespace CodeSample.Game
{
    /// <summary>
    /// Interface to comunicate with Updater engine.
    /// </summary>
    public interface IUpdater
    {
        void Register(IUpdateable objectToUpdate);
        void Unregister(IUpdateable objectToUpdate);
    }
}
