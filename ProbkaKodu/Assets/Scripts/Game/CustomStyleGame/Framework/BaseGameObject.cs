﻿using Zenject;

namespace CodeSample.Game
{
    /// <inheritdoc />
    /// <summary>
    /// Base building block of Game objects, used to give basic comunication with Game.
    /// </summary>
    public class BaseGameObject : BaseObject
    {
        private readonly ILifecycle _lifecycle;
        private readonly IPauseable _pausable;

        [Inject]
        public BaseGameObject(ILifecycle lifecycle, IPauseable pausable, IUpdater updater) : base(updater)
        {
            _lifecycle = lifecycle;
            _lifecycle.OnGameStarted += OnGameStarted;
            _lifecycle.OnGameEnded += OnGameEnded;

            _pausable = pausable;
            _pausable.OnGamePaused += OnGamePaused;
            _pausable.OnGameResumed += OnGameResumed;
        }

        /// <summary>
        /// OnGameStarted is called when the Game was started.
        /// Override to respond to OnGameStarted event.
        /// </summary>
        protected virtual void OnGameStarted()
        {
            Enabled = true;
        }

        /// <summary>
        /// OnGamePaused is called when the Game was paused.
        /// Override to respond to OnGamePaused event.
        /// </summary>
        protected virtual void OnGamePaused()
        {
            Enabled = false;
        }

        /// <summary>
        /// OnGameResumed is called when the Game was resumed.
        /// Override to respond to OnGameResumed event.
        /// </summary>
        protected virtual void OnGameResumed()
        {
            Enabled = true;
        }

        /// <summary>
        /// OnGameEnded is called when the Game ends.
        /// Override to respond to OnGameEnded event.
        /// </summary>
        protected virtual void OnGameEnded()
        {
            // intentionaly left blank
        }

        public override void Destroy()
        {
            base.Destroy();

            _lifecycle.OnGameStarted -= OnGameStarted;
            _lifecycle.OnGameEnded -= OnGameEnded;

            _pausable.OnGamePaused -= OnGamePaused;
            _pausable.OnGameResumed -= OnGameResumed;
        }
    }
}
