﻿using Zenject;

namespace CodeSample.Game
{
    /// <inheritdoc />
    /// <summary>
    /// Base building block of objects, used to give basic engine functionality.
    /// </summary>
    public class BaseObject : IUpdateable
    {
        private readonly IUpdater _updater;

        private bool _enabled = true;

        /// <summary>
        /// Enabled BaseObjects are Updated, disabled BaseObjects are not.
        /// </summary>
        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        [Inject]
        public BaseObject(IUpdater updater)
        {
            _updater = updater;
            _updater.Register(this);
        }

        /// <summary>
        /// Update is called every frame, if BaseObject is Enabled.
        /// </summary>
        public virtual void Update()
        {
            // intentionaly left blank
        }

        /// <summary>
        /// Used for cleanup before destroying object.
        /// </summary>
        public virtual void Destroy()
        {
            _updater.Unregister(this);
        }
    }
}
