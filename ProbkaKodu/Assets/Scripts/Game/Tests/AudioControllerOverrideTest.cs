﻿using UnityEngine;

namespace CodeSample.Game.Tests
{
    /// <summary>
    /// Class created to test and show how to use AudioController
    /// </summary>
    public class AudioControllerOverrideTest : AudioController
    {
        public void PlayAudioTest(AudioClip clip)
        {
            PlayAudio(clip);
        }

        public void PlayClipsInLoopTest(AudioClip[] clips)
        {
            PlayClipsInLoop(clips);
        }
    }
}
