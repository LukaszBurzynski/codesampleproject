﻿using UnityEngine;
using UnityEngine.TestTools;
using System.Collections;
using CodeSample.Game.Tests;
using Assert = UnityEngine.Assertions.Assert;

public class AudioControllerTests
{
    [UnityTest]
    public IEnumerator PlayAudioTest()
    {
        const float scale = 1f;
        Time.timeScale = scale;
        
        var audioControllerObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        audioControllerObject.AddComponent<AudioListener>();
        var audioController = audioControllerObject.AddComponent<AudioControllerOverrideTest>();
        var audioSource = audioController.GetComponent<AudioSource>();
        audioSource.pitch = scale;
        var clip = Resources.Load<AudioClip>("Audio/TestClips/Parts/TestMelody_part1");

        yield return null;

        if (clip != null)
        {
            audioController.PlayAudioTest(clip);
            yield return new WaitForSeconds(0.5f);

            Assert.IsTrue(audioSource.isPlaying);
            Assert.IsTrue(audioSource.volume > 0.4f && audioSource.volume < 0.6f);

            yield return new WaitForSeconds(clip.length);

            Assert.IsTrue(audioSource.volume > 0.99f);
            Assert.IsFalse(audioSource.isPlaying);
        }
        else
        {
            Debug.LogError("No clip loaded");
        }

        Time.timeScale = 1f;
        Object.Destroy(audioControllerObject);
    }
    
    [UnityTest]
    public IEnumerator PlayClipsInLoopTest()
    {
        const float scale = 1f;
        Time.timeScale = scale;
        
        var audioControllerObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        audioControllerObject.AddComponent<AudioListener>();
        var audioController = audioControllerObject.AddComponent<AudioControllerOverrideTest>();
        var audioSource = audioController.GetComponent<AudioSource>();
        audioSource.pitch = scale;
        var clips = Resources.LoadAll<AudioClip>("Audio/TestClips/Parts");

        yield return null;

        if (clips != null)
        {
            audioController.PlayClipsInLoopTest(clips);

            yield return null;
            Assert.IsTrue(audioSource.isPlaying);
            Assert.IsTrue(audioSource.clip == clips[0]);
            
            yield return new WaitForSeconds(audioSource.clip.length);
            Assert.IsTrue(audioSource.isPlaying);
            Assert.IsTrue(audioSource.clip == clips[1]);

            yield return new WaitForSeconds(audioSource.clip.length);
            Assert.IsTrue(audioSource.isPlaying);
            Assert.IsTrue(audioSource.clip == clips[0]);
        }
        else
        {
            Debug.LogError("No clips loaded");
        }

        Time.timeScale = 1f;
        Object.Destroy(audioControllerObject);
    }
}
