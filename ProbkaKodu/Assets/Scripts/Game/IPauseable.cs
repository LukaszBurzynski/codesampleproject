﻿using System;

namespace CodeSample.Game
{
    /// <summary>
    /// Interface for pause comunication.
    /// </summary>
    public interface IPauseable
    {
        event Action OnGamePaused;
        event Action OnGameResumed;
    }
}
