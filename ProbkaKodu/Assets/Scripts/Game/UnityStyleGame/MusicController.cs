﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace CodeSample.Game
{
    /// <summary>
    /// The purpose of MusicController is play correct music clips depending on the situation in game.
    /// Communication is handled by events received from different parts of application.
    /// </summary>
    public class MusicController : AudioController
    {
        private enum EGameMusic
        {
            Idle,
            Battle,
            AfterBattle,
            Death,
            AfterDeathIdle,
        }

        private ILifecycle _lifecycle;
        private IPauseable _pauseable;
        private IGameStages _player;

        private readonly Dictionary<EGameMusic, AudioClip[]> _audioClips = new Dictionary<EGameMusic, AudioClip[]>();

        [Inject]
        public void Construct(ILifecycle lifecycle, IPauseable pauseable, IGameStages player)
        {
            _lifecycle = lifecycle;
            _pauseable = pauseable;
            _player = player;
        }

        protected override void Start()
        {
            base.Start();

            InitializeAudioClips();
            InitializeLifecycleStages();
            InitializePlayerStages();
        }

        private void InitializeAudioClips()
        {
            _audioClips.Add(EGameMusic.Idle, Resources.LoadAll<AudioClip>("Audio/Music/Idle"));
            _audioClips.Add(EGameMusic.Battle, Resources.LoadAll<AudioClip>("Audio/Music/Battle"));
            _audioClips.Add(EGameMusic.AfterBattle, Resources.LoadAll<AudioClip>("Audio/Music/AfterBattle"));
            _audioClips.Add(EGameMusic.Death, Resources.LoadAll<AudioClip>("Audio/Music/Death"));
            _audioClips.Add(EGameMusic.AfterDeathIdle, Resources.LoadAll<AudioClip>("Audio/Music/AfterDeathIdle"));
        }

        private void InitializeLifecycleStages()
        {
            _lifecycle.OnGameStarted += OnGameStarted;
            _pauseable.OnGamePaused += OnGamePaused;
            _pauseable.OnGameResumed += OnGameResumed;
            _lifecycle.OnGameEnded += OnGameEnded;
        }

        private void InitializePlayerStages()
        {
            _player.OnBattleStarted += OnBattleStarted;
            _player.OnBattleEnded += OnBattleEnded;
            _player.OnDeath += OnDeath;
        }

        private void OnGameStarted()
        {
            PlayMusic(EGameMusic.Idle);
        }

        private void PlayMusic(EGameMusic music)
        {
            var clips = _audioClips[music];
            PlayClipsInLoop(clips);
        }

        private void OnGamePaused()
        {
            PauseAudio();
        }

        private void OnGameResumed()
        {
            ResumeAudio();
        }

        private void OnBattleStarted()
        {
            PlayMusic(EGameMusic.Battle);
        }

        private void OnBattleEnded()
        {
            PlayMusicInSequence(EGameMusic.AfterBattle, EGameMusic.Idle);
        }

        private void PlayMusicInSequence(EGameMusic music1, EGameMusic music2)
        {
            var clip1 = GetRandomClip(music1);
            var clips = _audioClips[music2];

            PlayClipsInSequenceWithSecondPartInLoop(clip1, clips);
        }

        private AudioClip GetRandomClip(EGameMusic music)
        {
            var clipsToDraw = _audioClips[music];
            return clipsToDraw[Random.Range(0, clipsToDraw.Length)];
        }

        private void OnDeath()
        {
            PlayMusicInSequence(EGameMusic.Death, EGameMusic.AfterDeathIdle);
        }

        private void OnGameEnded()
        {
            StopAudio();
        }

        private void OnDestroy()
        {
            _lifecycle.OnGameStarted -= OnGameStarted;
            _pauseable.OnGamePaused -= OnGamePaused;
            _pauseable.OnGameResumed -= OnGameResumed;
            _lifecycle.OnGameEnded -= OnGameEnded;

            _player.OnBattleStarted -= OnBattleStarted;
            _player.OnBattleEnded -= OnBattleEnded;
            _player.OnDeath -= OnDeath;
        }
    }
}
