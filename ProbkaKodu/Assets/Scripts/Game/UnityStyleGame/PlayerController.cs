﻿using System;
using UnityEngine;
using Zenject;

namespace CodeSample.Game
{
    /// <summary>
    /// Supporting code for UnityStyleGame, created to show how pausing works on objects in scene.
    /// </summary>
    public class PlayerController : PausableMonoBehaviour, IGameStages
    {
        public event Action OnBattleStarted;
        public event Action OnBattleEnded;
        public event Action OnDeath;

        [SerializeField] private float _playerSpeed = 10f;
        [SerializeField] private float _changeDirectionDistance = 10f;

        private ILifecycle _game;

        private Vector3 _direction = Vector3.right;

        [Inject]
        private void Construct(ILifecycle game)
        {
            _game = game;
        }

        private void Awake()
        {
            _game.OnGameStarted += OnGameStarted;
        }

        private void OnGameStarted()
        {
            enabled = true;
        }

        private void Update()
        {
            Move();
        }

        /// <summary>
        /// Dumy player movement for demonstration purposes
        /// </summary>
        private void Move()
        {
            ChoseDirection();

            transform.position += _direction * _playerSpeed * Time.deltaTime;
        }

        private void ChoseDirection()
        {
            if (transform.position.x < -_changeDirectionDistance)
            {
                _direction = Vector3.right;
            }
            else if (transform.position.x > _changeDirectionDistance)
            {
                _direction = Vector3.left;
            }
        }

        protected override void OnDestroy()
        {
            _game.OnGameStarted -= OnGameStarted;
            base.OnDestroy();
        }
    }
}
