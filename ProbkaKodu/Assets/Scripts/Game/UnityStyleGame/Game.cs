﻿using System;
using UnityEngine;

namespace CodeSample.Game
{
    /// <summary>
    /// The purpose of Game is to controll state of game lifecycle, by sending events to other componnents.
    /// </summary>
    public class Game : MonoBehaviour, ILifecycle, IPauseable
    {
        public event Action OnGameStarted;
        public event Action OnGamePaused;
        public event Action OnGameResumed;
        public event Action OnGameEnded;

        [SerializeField] private KeyCode _pauseButton;

        private bool _isGameStarted = false;
        private bool _isGamePaused = false;

        public void StartGame()
        {
            if (_isGameStarted == false)
            {
                _isGameStarted = true;

                GameStarted();
            }
        }

        private void GameStarted()
        {
            if (OnGameStarted != null)
            {
                OnGameStarted.Invoke();
            }
            else
            {
                Debug.LogWarning("Game: OnGameStarted doesn't have any events attached.");
            }
        }

        public void EndGame()
        {
            if (_isGameStarted)
            {
                _isGameStarted = false;

                GameEnded();
            }
        }

        private void GameEnded()
        {
            if (OnGameEnded != null)
            {
                OnGameEnded.Invoke();
            }
            else
            {
                Debug.LogWarning("Game: OnGameEnded doesn't have any events attached.");
            }
        }

        private void Update()
        {
            if (_isGameStarted && Input.GetKeyDown(_pauseButton))
            {
                PauseOrResume();
            }
        }

        private void PauseOrResume()
        {
            if (_isGamePaused)
            {
                _isGamePaused = false;
                ResumeGame();
            }
            else
            {
                _isGamePaused = true;
                PauseGame();
            }
        }

        private void ResumeGame()
        {
            if (OnGameResumed != null)
            {
                OnGameResumed.Invoke();
            }
            else
            {
                Debug.LogWarning("Game: OnGameResumed doesn't have any events attached.");
            }
        }

        private void PauseGame()
        {
            if (OnGamePaused != null)
            {
                OnGamePaused.Invoke();
            }
            else
            {
                Debug.LogWarning("Game: OnGamePaused doesn't have any events attached.");
            }
        }
    }
}
