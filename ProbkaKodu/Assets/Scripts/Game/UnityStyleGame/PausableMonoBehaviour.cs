﻿using UnityEngine;
using Zenject;

namespace CodeSample.Game
{
    /// <summary>
    /// The purpose of PausableMonoBehaviour is to give pause functionality to MonoBehaviour objects.
    /// Use this as middleman class to MonoBehaviour for objects that should pause they state.
    /// </summary>
    public class PausableMonoBehaviour : MonoBehaviour
    {
        private IPauseable _game;

        [Inject]
        public void Contruct(IPauseable game)
        {
            _game = game;
        }

        protected virtual void Start()
        {
            _game.OnGamePaused += OnGamePaused;
            _game.OnGameResumed += OnGameResumed;
        }

        /// <summary>
        /// Called when game was paused.
        /// </summary>
        protected virtual void OnGamePaused()
        {
            enabled = false;
        }

        /// <summary>
        /// Called when game was resumed.
        /// </summary>
        protected virtual void OnGameResumed()
        {
            enabled = true;
        }

        protected virtual void OnDestroy()
        {
            _game.OnGamePaused -= OnGamePaused;
            _game.OnGameResumed -= OnGameResumed;
        }
    }
}
