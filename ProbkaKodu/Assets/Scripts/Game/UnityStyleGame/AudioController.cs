﻿using System.Collections;
using UnityEngine;

namespace CodeSample.Game
{
    /// <summary>
    /// The purpose of AudioController is to manege comunication with audioSource,
    /// gives basic functionality to play audio clips with Fade In and Out.
    /// Fades should be used for clips that are not transiting correctly.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class AudioController : MonoBehaviour
    {
        [SerializeField] private float _maxVolume = 1f;

        private AudioSource _audioSource;

        private float _fadeInTime = 1f;
        private float _fadeOutTime = 1f;

        private bool _isPaused = false;

        protected virtual void Start()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        /// <summary>
        /// Fades Out from currently playing clip and starts to play given clip 1 time.
        /// </summary>
        /// <param name="clip"></param>
        protected void PlayAudio(AudioClip clip)
        {
            StopAllCoroutines();
            StartCoroutine(SwitchAudio(clip, false));
        }

        private IEnumerator SwitchAudio(AudioClip clip, bool loop)
        {
            if (_audioSource.isPlaying)
            {
                yield return FadeAudioOut();
            }

            PlayAudio(clip, loop);
            StartCoroutine(FadeAudioIn());
        }

        private IEnumerator FadeAudioOut()
        {
            var fadeOutSpeed = 1f / _fadeOutTime;

            while (_audioSource.volume > 0.01f)
            {
                yield return new WaitWhile(() => _isPaused);
                _audioSource.volume -= fadeOutSpeed * Time.deltaTime;
            }

            _audioSource.volume = 0f;
        }

        private void PlayAudio(AudioClip clip, bool loop)
        {
            _audioSource.loop = loop;
            _audioSource.clip = clip;
            _audioSource.Play();
        }

        private IEnumerator FadeAudioIn()
        {
            _audioSource.volume = 0f;

            var fadeInSpeed = 1f / _fadeInTime;

            while (_audioSource.volume < 0.99f)
            {
                yield return new WaitWhile(() => _isPaused);
                _audioSource.volume += fadeInSpeed * Time.deltaTime;
            }

            _audioSource.volume = _maxVolume;
        }

        /// <summary>
        /// Fades Out from currently playing clip and starts to play given clip in loop.
        /// </summary>
        /// <param name="clip"></param>
        protected void PlayAudioInLoop(AudioClip clip)
        {
            StopAllCoroutines();
            StartCoroutine(SwitchAudio(clip, true));
        }

        /// <summary>
        /// Plays clips in sequence and loops them from begining in table.
        /// Fades out and in between clips.
        /// </summary>
        /// <param name="clips"></param>
        protected void PlayClipsInLoop(AudioClip[] clips)
        {
            StopAllCoroutines();
            StartCoroutine(PlayClipsInLoopCoroutine(clips));
        }

        private IEnumerator PlayClipsInLoopCoroutine(AudioClip[] clips)
        {
            while (true)
            {
                yield return PlayClipsInSequenceCoroutine(clips);
            }
        }

        private IEnumerator PlayClipsInSequenceCoroutine(AudioClip[] clips)
        {
            foreach (var clip in clips)
            {
                yield return SwitchAudio(clip, false);

                var time = 0f;
                var timeToSwitchClip = clip.length - _fadeOutTime;

                while (time < timeToSwitchClip)
                {
                    yield return new WaitWhile(() => _isPaused);
                    time += Time.deltaTime;
                }
            }
        }

        /// <summary>
        /// Plays clips in sequence.
        /// Fades out and in between clips.
        /// </summary>
        /// <param name="clips"></param>
        protected void PlayClipsInSequence(AudioClip[] clips)
        {
            StopAllCoroutines();
            StartCoroutine(PlayClipsInSequenceCoroutine(clips));
        }

        /// <summary>
        /// Plays first clip and loops second clips in sequence.
        /// Fades out and in between clips.
        /// </summary>
        /// <param name="clips"></param>
        protected void PlayClipsInSequenceWithSecondPartInLoop(AudioClip soloClip, AudioClip[] loopClips)
        {
            StopAllCoroutines();
            StartCoroutine(PlayClipsInSequenceWithLastInLoopCoroutine(soloClip, loopClips));
        }

        private IEnumerator PlayClipsInSequenceWithLastInLoopCoroutine(AudioClip soloClip, AudioClip[] loopClips)
        {
            yield return SwitchAudio(soloClip, false);
            yield return new WaitForSeconds(soloClip.length);

            yield return PlayClipsInLoopCoroutine(loopClips);
        }

        /// <summary>
        /// Pauses playing the clip.
        /// </summary>
        protected void PauseAudio()
        {
            _isPaused = true;
            _audioSource.Pause();
        }

        /// <summary>
        /// Resumes playing of paused audio.
        /// The clip needs to be already set.
        /// </summary>
        protected void ResumeAudio()
        {
            if (_audioSource.clip != null)
            {
                _isPaused = false;
                _audioSource.UnPause();
            }
            else
            {
                Debug.LogWarning("AudioController: Trying to RESUME audioSource with no clip.", gameObject);
            }
        }

        /// <summary>
        /// Fades out audio and stops it completely
        /// </summary>
        protected void StopAudio()
        {
            if (_audioSource.isPlaying)
            {
                StopAllCoroutines();
                StartCoroutine(StopPlayingAudioCoroutine());
            }
            else
            {
                Debug.LogWarning("AudioController: Trying to STOP already stoped audioSource.", gameObject);
            }
        }

        private IEnumerator StopPlayingAudioCoroutine()
        {
            yield return FadeAudioOut();

            _audioSource.Stop();
        }
    }
}
