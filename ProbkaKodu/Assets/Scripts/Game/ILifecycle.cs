﻿using System;

namespace CodeSample.Game
{
    /// <summary>
    /// Interface for basic game lifecycle comunication.
    /// </summary>
    public interface ILifecycle
    {
        event Action OnGameStarted;
        event Action OnGameEnded;
    }
}
