﻿using System;

namespace CodeSample.Game
{
    /// <summary>
    /// Interface for game stages comunication.
    /// </summary>
    public interface IGameStages
    {
        event Action OnBattleStarted;
        event Action OnBattleEnded;
        event Action OnDeath;
    }
}
